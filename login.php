<?php
define("TITLE", "Login");
session_start();
include 'includes/header.php';

//declare error and instantiate Login
$error = '';
$_SESSION['username']='';
$_SESSION['password']= '';
$login  = new App\Controllers\Login();

//Check the login button is pressed and modification of variables
if (isset($_POST['login'])) {
    $username = htmlspecialchars(filter_var($_POST['username']), FILTER_SANITIZE_STRING);
    $pwd = strval($_POST['password']);
    $user_check = $login->checkUser($username);
    $real_pwd = strval($user_check['password']);
    $acception =  password_verify($pwd, $real_pwd);

   
        //check the matching password
        if (!$acception) {
            $_SESSION['username'] = null;
            $error = 'Username or password is incorrect!';
        } elseif ($acception) {
            $_SESSION['username'] = $username;
            if (!$user_check['password_changed']) {
                $_SESSION['password'] = $_POST['password'];
                header('Location: confirmlogin.php');
            } else {
                header("Location: home.php");
            }
        }
    }


?>
<main class="container h-100 container-fluid">
    <div class="row h-100 justify-content-center">
        <div class="col-10 col-md-8 col-lg-6 p-2 m-2 bg-light rounded border">
            <h2 class="text-center"> Log in </h2>
            <form action="login.php" method=POST>
                <div class="form-group">
                    <label for="username">Username</label>
                    <input class="form-control username" type="text" id="username" name="username" placeholder="username" value="<?php echo $username ?? ""; ?>">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input class="form-control password" type="password" id="password" name="password" placeholder=Password>
                </div>
                <button class="btn btn-primary" type="submit" name='login'>Log in</button>
                <span class="text-danger"> <?php echo $error; ?></span>
                <p class="text-danger text-center"><?php echo $_GET['problem'] ?? "" ?></p>
            </form>
        </div>
    </div>
</main>

<?php

include 'includes/footer.php';

?>