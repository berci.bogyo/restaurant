<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Controllers\DB;
use PDO;
use PDOException;

class SignUp extends DB
{
    /**
     * registration
     *
     * @param  mixed $first_name
     * @param  mixed $last_name
     * @param  mixed $email
     * @param  mixed $username
     * @param  mixed $pwd
     * @return bool Return the successfully sign up
     */
    public function registration(string $first_name, string $last_name, string $email, string $username, string $pwd): bool
    {
        try {
            $sql = "INSERT INTO users (username, email, password, first_name, last_name, date, password_changed) VALUES (:username, :email, :password, :first_name, :last_name, current_timestamp, false);";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute(
                [
                    'username' => $username,
                    'email' => $email,
                    'password' => $pwd,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                ]
            );
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    /**
     * existingUsername
     *
     * @param  string $username
     * @return array
     */
    public function existingUsername(string $username): array
    {
        try {
            $sql = "SELECT count(*) FRoM users WHERE username = :username;";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute(['username' => $username]);
            $existingUser = $stmt->fetch(PDO::FETCH_ASSOC);
            return $existingUser;
        } catch (PDOException $e) {
            return array();
        }
    }
    /**
     * existingEmail
     *
     * @param  string $email
     * @return string
     */
    public function existingEmail(string $email): array
    {
        try {
            $sql = "SELECT count(*) FROM users WHERE email = :email;";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute(['email' => $email]);
            $existingEmail = $stmt->fetch(PDO::FETCH_ASSOC);
            return $existingEmail;
        }catch (PDOException $e) {
            return array();
        }
    }
}
