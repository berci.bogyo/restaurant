<?php

namespace App\Controllers;

use PDO;
use PDOException;

class DB
{
    private $dbHost = "localhost";
    private $dbUser = "berci";
    private $dbPort = "5432";
    private $dbPassword = "nincsjelszo";
    private $dbName = "restaurant";
    protected $conn;

    public function __construct()
    {
        try {
            $dsn = "pgsql:host=" . $this->dbHost . ";port=" . $this->dbPort . ";dbname=" . $this->dbName;
            $this->conn = new PDO($dsn, $this->dbUser, $this->dbPassword);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die('You can not access the applicaton');
        }
    }
}
