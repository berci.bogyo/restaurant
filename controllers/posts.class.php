<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Controllers\DB;
use PDO;
use PDOException;

class Post extends DB
{
    /**
     * posts
     *
     * @param  string $username
     *
     * The posts of posted by the $username
     */
    public function posts(string $username)
    {
        try {
            $sql = "SELECT post, p.date as date FROM posts  as p JOIN users as u ON p.username = u.username WHERE p.username = :username ORDER BY p.date";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute(['username' => $username]);
            $post = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $post;
        } catch (PDOException $e) {
            return "The page can not load the posts!";
        }
    }

    /**
     * postSend
     *
     * @param  mixed $username
     * @param  mixed $message
     * @return string
     */
    public function postSend(string $username, string $message): string
    {
        try {

            $sql = "INSERT INTO posts (userid, username, post, date) VALUES ((SELECT user_id FROM users WHERE username = :username),:username, :post, current_timestamp);";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute(['username' => $username, 'post' => $message]);
            return "sent=The post has been sent";
        } catch (PDOException $e) {
            return "notsent=The page can not send the post!";
        }
    }
}
