<?php
namespace App\Controllers;

use App\Controllers\DB;
use PDO;
use PDOException;

class Menu extends DB
{    
    /**
     * getMenu
     *
     * @param  int $limit
     * @return array
     */
    public function getMenu(int $limit)
    {
        try {
            $sql = "SELECT * FROM menu LIMIT :limit";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute(['limit'=> $limit]);
            $getMenu = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $getMenu;
        } catch (PDOException $e) {
            return array();
        }  
    }    
    /**
     * getAllMenu
     *
     * @return array
     */
    public function getAllMenu()
    {
        try {
            $sql = "SELECT count(*) FROM menu";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $getAllMenu = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $getAllMenu;
        } catch (PDOException $e) {
            return array();
        }  
    }    
    /**
     * getPopularMenu
     *
     * @return mixed
     */
    public function getPopularMenu()
    {
        try {
            $sql = "SELECT * FROM menu WHERE popular = true";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $getPopular = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $getPopular;
        } catch (PDOException $e) {
            return array();
        }  
    }
}