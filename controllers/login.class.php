<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Controllers\DB;
use PDO;
use PDOException;

class Login extends DB
{
    /**
     * checkUser
     *
     * @param  string $username
     *
     */
    public function checkUser(string $username)
    {
        try {
            $sql = "SELECT * FROM users WHERE username = :username ";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute(['username' => $username]);
            $checkUser = $stmt->fetch(PDO::FETCH_ASSOC);
            return $checkUser;
        } catch (PDOException $e) {
            return header('Location: login.php?problem=Problem with the login!');
        }
    }
    public function passwordChange(string $username, string $password)
    {
        try {
            $sql = "UPDATE users SET password = :password, password_changed = true WHERE username = :username";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute(['username'=>$username, 'password' =>$password]);
            $stmt->fetch(PDO::FETCH_ASSOC);
            return true;
        } catch (PDOException $e) {
            return header('Location: confirmlogin.php?problem=Problem with the password change!');
        }
    }
}
