# Restaurant App

Installation guide for the project.

## Installation
If you have not php on your machine you should install first!


### Clone or download the files.

You have to place the files the proper directory
```sh
$ sudo mv <cloned folder> /var/www/html
$ cd /var/www/html/<cloned folder>
```
### Postgres install 

You have to install [postgres]((https://www.postgresql.org/download/linux/ubuntu/)
And you have to create the same databas with the same password as in the phinx.yml

```YML
development:
        adapter: pgsql
        host: localhost
        name: restaurant
        user: berci
        pass: nincsjelszo
        port: 5432
        charset: utf8
```

### Run composer

If you installed composer run the following:
You can find the composer setup on your machhine: [composer](https://getcomposer.org/)
```sh
$ composer install
```

### Run the autoload for php classes

```sh
$ composer dump-autoload -o
```

### Database migration

You need [phinx](https://phinx.org/)  
```sh
$ vendor/bin/phinx migrate -e development
```

### Seed the database 
Run the following command: 
```sh
$ vendor/bin/phinx seed:run -s UserSeed -s PostsSeed -s MenuSeed
```

### NPM install

Install the npm dependencies
If you do not have [Node.js](https://nodejs.org/en/) installed, first you have to do.

```sh
$ npm install 
```
### Set the css and javascript 

Run [gulp](https://gulpjs.com/) tasks with 

```sh
$ gulp 
```

If you want to use live compiler run: 

```sh
$ gulp watch
```

### Run the server 
If you do not have apache2 run this:
```sh
$ sudo apt install apache2
```
After your app directory is in /var/www/html/

```sh
$ sudo service apache2 start
```

### Open in the browser

```sh
localhost/restaurant/
```

