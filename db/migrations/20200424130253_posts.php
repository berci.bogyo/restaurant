<?php

use Phinx\Migration\AbstractMigration;

class Posts extends AbstractMigration
{
    public function change()
    {
        $posts = $this->table('posts',['id' => false]);
        $posts->addColumn('userid', 'integer')
            ->addColumn('username', 'string', ["limit" => 20])
            ->addColumn('post', 'text', ["limit" => 500])
            ->addColumn('date', 'datetime')
            ->addIndex(['userid'])
            ->addForeignKey('userid','users',['user_id'])
            ->create();
    }
}
