<?php

use Phinx\Migration\AbstractMigration;

class Menu extends AbstractMigration
{
    public function change()
    {
        $posts = $this->table('menu',['id' => false]);
        $posts->addColumn('menuid', 'integer')
            ->addColumn('dish', 'string', ["limit" => 100])
            ->addColumn('price', 'integer', ["limit" => 100])
            ->addColumn('picture', 'string')
            ->addColumn('popular', 'boolean')
            ->create();
    }
}
