<?php

use Phinx\Migration\AbstractMigration;

class Users extends AbstractMigration
{
    public function change()
    {
        $users = $this->table('users',['id' => false,  'primary_key' => ['user_id']]);
        $users->addColumn('user_id', 'integer',['identity'=>true])
            ->addColumn('username', 'string', ["limit" => 20])
            ->addColumn('email', 'string', ["limit" => 50])
            ->addColumn('password', 'string', ["limit" => 255])
            ->addColumn('first_name', 'string', ["limit" => 50])
            ->addColumn('last_name', 'string', ["limit" => 50])
            ->addColumn('date', 'datetime')
            ->addColumn('password_changed', 'boolean')
            ->addIndex(['user_id'], ['unique'=>true] )
            ->addIndex(['username'], ['unique'=>true] )
            ->addIndex([ 'email'], ['unique'=>true] )
        ->create();
        
    }
}
