<?php


use Phinx\Seed\AbstractSeed;

class MenuSeed extends AbstractSeed
{
    public function run()
    {
        $faker = Faker\Factory::create();
        $faker->addProvider( new \FakerRestaurant\Provider\en_US\Restaurant($faker));
        $data=[];
        for ($i = 0; $i < 30; $i++) {
            $data[] = [
                'menuid'=>$i,
                'dish' => $faker->foodName(),
                'price'=>$faker->biasedNumberBetween($min = 1500, $max = 3000),
                'picture'=>'dist/images/'.rand(1, 10).".jpg",
                'popular'=>$faker->biasedNumberBetween($min = 0, $max = 1)
            ];
        }
        $this->table('menu')->insert($data)->save();
    }
}
