<?php


use Phinx\Seed\AbstractSeed;

class PostsSeed extends AbstractSeed
{
    public function run()
    {
        $data = [
            [  
                'userid'=>1,
                'post'=>'Nem tudok rendelni!',
                'username' => 'nagylaci',
                'date'=> date("Y-m-d h:i:sa", time())
            ],
            [   
                'userid'=>1,
                'post'=>'Nem jó az oldal!',
                'username' => 'nagylaci',
                'date'=> date("Y-m-d h:i:sa", time())
            ],
            [  
                'userid'=>2,
                'post'=>'Nem érkezett meg a pizzám!',
                'username' => 'kispisti',
                'date'=> date("Y-m-d h:i:sa", time())
            ],
            [   
                'userid'=>2,
                'post'=>'Nem tudok bejelentkezni másik felhasználóval!',
                'username' => 'kispisti',
                'date'=> date("Y-m-d h:i:sa", time())
            ]
        ];
        $posts = $this->table('posts');
        $posts->insert($data)
            ->saveData();
    }
}
