<?php


use Phinx\Seed\AbstractSeed;

class UserSeed extends AbstractSeed
{
    public function run()
    {
        $data = [
            [  
                'username' => 'nagylaci',
                'email' => 'lacika@gmail.com',
                'password' => password_hash('jelszo123', PASSWORD_ARGON2I),
                'first_name' => 'Nagy',
                'last_name' => 'László',
                'password_changed'=> 0,
                'date'=> date("Y-m-d h:i:sa", time()),
            ],
            [   
                'username' => 'kispisti',
                'email' => 'pista@gmail.com',
                'password' => password_hash('password', PASSWORD_ARGON2I),
                'first_name' => 'Kiss',
                'last_name' => 'István',
                'password_changed'=> 0,
                'date'=> date("Y-m-d h:i:sa", time())
            ]
        ];
        $posts = $this->table('users');
        $posts->insert($data)
            ->saveData();
    }
}
