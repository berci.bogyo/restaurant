<?php
define("TITLE", "Menus");
session_start();
include 'includes/header.php';

?>
<div class="skeleton">
    <div class='skeleton-nav'>
        <ul class="nav">
            <li class='skeleton-nav-items nav-item'></li>
            <li class='skeleton-nav-items nav-item'></li>
            <li class='skeleton-nav-items nav-item'></li>
            <li class='skeleton-nav-items nav-item'></li>
        </ul>
    </div>
    <div class='skeleton-header'></div>
    <div class='skeleton-page row '>
        <div class='skeleton-page-card m-1 p-1 '></div>
        <div class='skeleton-page-card m-1 p-1  '></div>
        <div class='skeleton-page-card m-1 p-1 '></div>
        <div class='skeleton-page-card m-1 p-1  '></div>
        <div class='skeleton-page-card  m-1 p-1 '></div>
        <div class='skeleton-page-card  m-1 p-1 '></div>
    </div>
</div>
<main class="container-fluid h-100 text-center">
<h2 class="m-3 text-white">The menu list!</h2>

    <div id="menus" class="container-fluid" >
        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-5" data-boxes-container>
        </div>
       <script type="text/template" data-template>
       <div class="col mb-2" >
           <div class="card menu m-1 p-1 d-flex flex-column h-100 bg-white">
           <div class="img-container"><img class="menu-pic img-fluid" src="-picture-" alt="food"></div>
               <p class="card-title">
                   -dish-
               </p>
                <p class="card-footer text-primary mt-auto">
                    -price- forint
                </p>
           </div>
       </div>
    </script>
    
    </div>

    <button id=loadMore class='m-5 btn btn-success text-white' type='button'>Load more dishes</button>
</main>

<script type='text/javascript' src='dist/js/load.js?v=<?php echo time(); ?>'></script>
<?php
include 'includes/footer.php';
?>