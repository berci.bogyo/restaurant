const loadMore = document.getElementById('loadMore');
let dataBox = $("[data-box]");
let boxTemplate = $("[data-template]").html();
let container = $("[data-boxes-container]");

let limit = 0;
    window.addEventListener("load", list);
    loadMore.addEventListener("click", list);

function list(e) {
    e.preventDefault()
    limit += 10;
    
    $.ajax({
        method: "POST",
        url: "includes/loadmore.php",
        data: {
            limit: limit
        },
        success: function (data) {
            let jsonObj = JSON.parse(data);
            let array = jsonObj[1].slice(Math.max(jsonObj[1].length - 10, 0));
            if (jsonObj[0].length === 0 || jsonObj[1].length === 0) {
                alert('Cant load the menus now!');
                loadMore.style.visibility = 'hidden';
            }
            let allRecords = jsonObj[0][0]['count'];

            if (jsonObj[1].length === allRecords) {
                loadMore.style.visibility = 'hidden';
            }
            let newItems="";
            array.forEach(menu => {
                newItems+=boxTemplate
                .replace(/-dish-/, menu['dish'])
                .replace(/-picture-/, menu['picture'])
                .replace(/-price-/, menu['price'])
            });
            container.append(newItems);
        },
        error: function (request, status, error) {
            alert(request.responseText);
        }
    })
};