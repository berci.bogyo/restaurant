<?php
define("TITLE", "Sign up");

include 'includes/header.php';


$error = false;

$registration  = new App\Controllers\SignUp();

//Check the signup button is pressed and modification of variables
if (isset($_POST['sign-up'])) {
    $first_name = htmlspecialchars(filter_var($_POST['first-name'], FILTER_SANITIZE_STRING));
    $last_name = htmlspecialchars(filter_var($_POST['last-name'], FILTER_SANITIZE_STRING));
    $username = htmlspecialchars(filter_var($_POST['username']), FILTER_SANITIZE_STRING);
    $email = htmlspecialchars(filter_var($_POST['email'], FILTER_SANITIZE_EMAIL));
    $pwd = $_POST['password'];
    $re_pwd = $_POST['re-password'];

    //Query the existing username and email
    $exist_username = $registration->existingUsername($username);
    $exist_email = $registration->existingEmail($email);
    if (empty($exist_email) || empty($exist_username)) {
        $failed = 'Problem with the Sign up!';
    } else {
        //error handlings
        if (empty($first_name)) {
            $first_name_req = "Firstname is required!";
            $error = true;
        }
        if (empty($last_name)) {
            $last_name_req = "Lastname is required!";
            $error = true;
        }
        if (empty($username)) {
            $username_req = "Username is required!";
            $error = true;
        }
        if (empty($email)) {
            $email_req = "Email is required!";
            $error = true;
        }
        if (empty($pwd)) {
            $pwd_req = "Password is required!";
            $error = true;
        }
        if (empty($re_pwd)) {
            $re_pwd_req = "Retype password is required!";
            $error = true;
        } elseif (!$error) {
            if ($exist_username['count']) {
                $userexist = "This username already exist!";
            }
            if ($exist_email['count']) {
                $emailexist = "The email is used!";
            } elseif (!$exist_email['count'] && !$exist_username['count']) {

                if ($pwd != $re_pwd) {
                    $pwderror = 'Set the same password';
                }
                if (strlen($pwd) < 5) {
                    $pwdlength = 'Minimum 5 characters!';
                } elseif ($pwd === $re_pwd && strlen($pwd) >= 5) {
                    $password = password_hash($pwd, PASSWORD_ARGON2I);
                    $sign = $registration->registration($first_name, $last_name, $email, $username, $password);
                    if (!$sign) {
                        $failed = "Registration was not success!";
                    } else {
                        $success = "Success! Redirecting to the Login page!";
                        header('Refresh:2 ; URL=login.php');
                    }
                }
            }
        }
    }
}

?>
<main class=" h-100 container">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-10 col-md-8 col-lg-6 p-2 m-2 bg-light rounded border">
            <h2 class="text-center">Sign up</h2>
            <span class="text-success text-center">
                <?= $success ?? ""; ?>
            </span>
            <form method="POST" action="">
                <div class="form-group ">
                    <label for="firstName">First Name
                        <span class="text-danger"> <?= $first_name_req ?? "*"; ?> </span>
                    </label>
                    <input class="form-control username" type="text" id="firstName" name="first-name" placeholder="First name" value="<?php echo $first_name ??  ""; ?>" required>
                </div>
                <div class="form-group ">
                    <label for="lastName">Last Name
                        <span class="text-danger"> <?= $last_name_req ?? "*"; ?></span>
                    </label>
                    <input class="form-control username" type="text" id="lastName" name="last-name" placeholder="Last name" value="<?php echo $last_name ?? ""; ?>" required>
                </div>
                <div class="form-group ">
                    <label for="username">Username
                        <span class="text-danger">
                            <?= $username_req ?? "*"; ?>
                            <?= $userexist ?? ""; ?>
                        </span>
                    </label>
                    <input class="form-control username" type="text" id="username" name="username" placeholder="Username" value="<?php echo $username ?? ""; ?>" required>
                </div>
                <div class="form-group ">

                    <label for="email">E-mail
                        <span class="text-danger"> <?= $email_req ?? "*"; ?>
                            <?= $emailexist ?? ""; ?>
                        </span>
                    </label>
                    <input class="form-control email" type="email" id="email" name="email" placeholder="e-mail" value="<?php echo $email ?? ""; ?>" required>
                </div>
                <div class="form-group ">
                    <label for="password">Password
                        <span class="text-danger">
                            <?= $pwd_req ?? "*"; ?>
                            <?= $pwdlength ?? ""; ?>
                        </span>
                    </label>
                    <input class="form-control password" type="password" id="password" name="password" placeholder=Password required>
                </div>
                <div class="form-group ">
                    <label for="re-password">Retype password
                        <span class="text-danger">
                            <?= $re_pwd_req ?? "*"; ?>
                            <?= $pwderror ?? ""; ?>
                        </span>
                    </label>
                    <input class="form-control password" type="password" id="re-password" name="re-password" placeholder="Retype password" required>
                </div>
                <button class="btn btn-primary" type="submit" name='sign-up'>Sign up</button>

                <span class="text-danger"> <?php echo $failed ?? ""; ?> </span>
            </form>
        </div>
    </div>
</main>

<?php

include 'includes/footer.php';
?>