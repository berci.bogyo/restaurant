const gulp = require('gulp');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');
const sass = require('gulp-sass');
const imagemin = require('gulp-imagemin');

gulp.task('message', async()=> {
    return console.log('Gulp works!');
});

gulp.task('imageMin', ()=>
    gulp.src('src/images/*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/images')));

gulp.task('minify', async()=>{
    gulp.src('src/js/*.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
});
gulp.task('sass', async()=>{
    gulp.src('src/style/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('dist/css'))
});

gulp.task('default', gulp.parallel(['minify','sass', 'imageMin']));

gulp.task('watch', ()=> {
    gulp.watch('src/js/*.js', gulp.series('minify'));
    gulp.watch('src/style/*scss', gulp.series('sass'));
});

