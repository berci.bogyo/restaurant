<?php
define("TITLE", "Home page");
session_start();
include('includes/header.php');

//if the user is not logged in
if (empty($_SESSION['username'])) {
    echo "<h2 class='text-center text-white'>Log in first!</h2>";
} else {
    $username = $_SESSION['username'];
    require_once 'includes/showpost.inc.php';
?>
    <main class="container h-100">
        <h2 class="text-center text-white"> You are Logged in as <?php echo $username; ?> </h2>
        <form action="includes/logout.inc.php" method="post">
            <button class=" btn btn-danger" name="logout" type="submit">Logout</button>
        </form>
        <div class="d-flex flex-column">
            <?php

            //there is no post or loop through the showPosts array
            if (empty($show_posts)) {
                echo "<p class='text-white'>You have not got any posts, create one!</p>";
            }
            if (is_array($show_posts)) {
                foreach ($show_posts as $post) {
                    echo "<div class='p-2 m-2 border bg-light align-items-center justify-content-center rounded'>"
                        . "<p>"
                        . $post['post']
                        . "</p><p>"
                        . date("Y-m-d G:i:s", strtotime($post['date']))
                        . "</p>"
                        . "</div>";
                }
            } else {
                echo "<span class='text-danger'>" . $show_posts . "</span>";
            } ?>
        </div>
        <div class="row h-100 justify-content-center">
        <div class="col-10 col-md-8 col-lg-6 p-2 m-2 bg-light rounded border">
        <form action="includes/sendpost.inc.php" method="post">
            <div class="form-group ">
                <label  for="post">Message to the Chef:</label><span class="text-danger"> <?php echo $_GET['empty'] ?? $_GET['notsent'] ?? "*"; ?> </span>
                <span class="text-success"> <?php echo $_GET['sent'] ?? ""; ?> </span>
                <textarea class="form-control" cols="10" rows="4" type="text" id="username" name="post" placeholder="Write message for the chef!"></textarea>
            </div>
            <button class="btn btn-success" name="send" type="submit">Send post</button>
        </form>
        </div>
        </div>
    </main>
<?php
}
include('includes/footer.php');

?>