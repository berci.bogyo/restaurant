<?php
define("TITLE", "Welcome");
session_start();
include 'includes/header.php';
if (!isset($_GET['disconnect'])) {
    $_GET['disconnect'] = "";
} else {
    $_GET['disconnect'] = "Currently the page is not working properly!";
}

$popular = new App\Controllers\Menu();
    $menus = $popular->getPopularMenu();
?>
<div class="skeleton">
<div class='skeleton-nav'>
    <ul class="nav">
    <li class='skeleton-nav-items nav-item'></li>
    <li class='skeleton-nav-items nav-item'></li>
    <li class='skeleton-nav-items nav-item'></li>
    <li class='skeleton-nav-items nav-item'></li>
    </ul>
</div>
<div class='skeleton-header'></div>
<div class='skeleton-page row '>
<div class='skeleton-page-card m-1 p-1 '></div>
<div class='skeleton-page-card m-1 p-1  '></div>
<div class='skeleton-page-card m-1 p-1 '></div>
<div class='skeleton-page-card m-1 p-1  '></div>
<div class='skeleton-page-card  m-1 p-1 '></div>
</div>
</div>
<main class="text-center h-100 container-fluid">
    <h1 class="m-3 text-light">Welcome to the Forest Restaurant</h1>
    <h2 class="m-3 text-light">This is the Best Restaurant you have ever seen</h2>
    <h3 class="m-3 text-light"> Top orders: </h3>
    <h2 class="text-danger"> <?php echo $_GET['disconnect'] ?? ""; ?> </h2>

    <div id="carousel-menu" class="carousel carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <?php
            if (isset($menus)) {
                $counter = 0;
                for ($j = 0; $j < count($menus); $j++) {
                    $counter++;
                    if ($counter % 5 === 0) {
                        if ($counter === 5) {
                            echo " <div class='carousel-item active' >";
                            echo "<div class='row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-5'>";
                            for ($i = 0; $i < $counter; $i++) {
                                echo "<div class='col mb-2'><div class='card m-1 p-1 d-flex flex-column h-100 bg-white'><div class='img-container'><a href='menu.php'><img class='img-fluid' src='".$menus[$i]['picture']. "'></a></div>
                             <p>" . $menus[$i]['dish'] . "</p >
                            </div></div>";
                            }
                            echo "</div>";
                            echo "</div>";
                        } elseif ($counter > 5) {
                            echo "<div class='carousel-item'>";
                            echo "<div class='row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-5'>";
                            for ($i = $counter - 5; $i < $counter; $i++) {
                                echo "<div class='col mb-2'><div class='card m-1 p-1 d-flex flex-column h-100 bg-white'><div class='img-container'><a href='menu.php'><img class='img-fluid' src='".$menus[$i]['picture']. "'></a></div>
                                <p>" . $menus[$i]['dish'] . "</p >
                               </div></div>";
                            }
                       
                            echo "</div>";
                            echo "</div>";
                        }
                    }
                }
            }
            ?>
        </div>
    </div>
    
</main>
<?php
include 'includes/footer.php';
?>