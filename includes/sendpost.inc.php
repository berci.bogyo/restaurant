<?php
session_start();

require '../vendor/autoload.php';

//instantiate the Post and insert new post
$username = $_SESSION['username'];
$posts = new App\Controllers\Post();
if (isset($_POST['send'])) {
    $message = htmlspecialchars($_POST['post']);
    if (empty($message)) {

        header('Location:../home.php?empty=You can not send empty post!');
    } else {
        //$date = date("Y-m-d h:i:sa", time());
        $sent = $posts->postSend($username, $message);
        if (is_array($posts)) {
            header('Location: ../home.php?' . $sent);
        } else {
            header('Location: ../home.php?' . $sent);
        }
    }
}
