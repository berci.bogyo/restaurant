
 <?php require 'vendor/autoload.php'; ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> <?php echo TITLE; ?> </title>

    <script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js?v=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="node_modules/bootstrap/dist/js/bootstrap.min.js?v=<?php echo time(); ?>"></script>
    <link rel="stylesheet" href="dist/css/style.css?v=<?php echo time(); ?>" >
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css?v=<?php echo time(); ?>">
</head>

<body>
<nav class="navbar fixed-top navbar-expand-lg bg-white text-dark">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-header"> &#9776; </button>
<div class="navbar-collapse collapse navbar-nav" id="navbar-header">
<ul class="navbar-nav mr-auto">
        <li class="nav-item">
                <a class="nav-link" href="index.php">Welcome</a>
            </li>
           
            <?php 
            if (!empty($_SESSION['username'])) {
                $username = ucfirst($_SESSION['username']);
                echo " <li class='nav-item'>"
                ."<p class='nav-link'><strong>"
                .$username
                ."</strong></p>"
                ."</li>";
            } else {
                echo " <li class='nav-item'>"
                ."<a class='nav-link' href='login.php'>Login</a>"
                ."</li>
                <li class='nav-item'>"
                ."<a class='nav-link' href='signup.php'>Signup</a>"
                ."</li>";
            }

            ?>
            <li class="nav-item">
                <a class="nav-link" href="home.php">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="menu.php">Menu</a>
            </li>
        </ul>
        </div>
        <h2 class="text-primary title-header">Forest Restaurant</h2>      
    </nav>
    <div class="background"></div>


   