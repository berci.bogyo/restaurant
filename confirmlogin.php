<?php
define("TITLE", "Login");
include 'includes/header.php';
session_start();

$error = '';
$login  = new App\Controllers\Login();
$username =  $_SESSION['username'];
if (isset($_POST['change'])) {
    $pwd = strval($_POST['password']);
    $re_pwd = strval($_POST['re-password']);
    if (empty($pwd)) {
        $pwd_req = 'Required password';
    }
    if (empty($re_pwd)) {
        $re_pwd_req = 'Required retype password!';
    } else {
        if ($pwd === $re_pwd) {
            if ($pwd != $_SESSION['password']) {
            
            $change_pwd = $login->passwordChange($username, password_hash($pwd, PASSWORD_ARGON2I));
            if ($change_pwd) {
                $_SESSION['password'] = null;
                header('Location: home.php');
            } else {
                $error = 'The password is not changed yet!';
            }
        } else {
            $error = 'You cant set your old password again!';
        }
        } else {
            $error = 'Type the same passwords!';
        }
    }
}

?>

<main class="container">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-10 col-md-8 col-lg-6 p-2 m-2 bg-light rounded border">
            <h3 class="text-center"> You logged in first time! You have to change your password! </h3>
            <form action="confirmlogin.php" method=POST>
                <div class="form-group ">
                    <label for="password">New password</label><span class="text-danger"> <?= $pwd_req ?? "*"; ?></span>
                    <input class="form-control username" type="password" id="password" name="password" placeholder="New Password">
                </div>
                <div class="form-group">
                    <label for="re-password">New Password Again</label><span class="text-danger"> <?= $re_pwd_req ?? "*"; ?></span>
                    <input class="form-control password" type="password" id="re-password" name="re-password" placeholder="Retype New Password">
                </div>
                <button class="btn btn-primary" type="submit" name='change'>Change Password</button>
                <span class="text-danger"> <?php echo $error; ?></span>
                <p class="text-danger text-center"><?php echo $_GET['problem'] ?? "" ?></p>
            </form>
        </div>
    </div>
</main>

<?php

include 'includes/footer.php';

?>